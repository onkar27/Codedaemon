# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render,get_object_or_404
from home.models import Contest,Con_signup
from login.models import User
from django.utils import timezone


def get_u(request):
	try:
		u = request.session['user']
	except:
		return ''
	return u

def index(request):
	try:
		con=Contest.objects.filter(end_time__lt=timezone.now())
		con_signup = Con_signup.objects.filter(user=get_object_or_404(User,username=get_u(request)))
		contestlist=[]
		for x in con_signup:
			contestlist.append(x.contest)
	except Contest.DoesNotExist:
		raise Http404("contest does not exits")
	return render(request,'index.html',{'con':con,'con_sign':contestlist,'arr':['','','active','','']})
