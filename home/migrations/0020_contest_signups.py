# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-03-30 23:10
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0019_auto_20180309_2151'),
    ]

    operations = [
        migrations.AddField(
            model_name='contest',
            name='signups',
            field=models.IntegerField(default=0),
        ),
    ]
