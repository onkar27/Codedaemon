from django.contrib import admin
from .models import Profile,AdminUser
admin.site.register(Profile)
admin.site.register(AdminUser)
